#include <stdio.h>
#include <string.h>
// ===============================
// 	Definicion de variables 
// ===============================
typedef enum { false, true } bool;
#define CANT_PERSONAS 10
#define MAX_LEN 30

// ===============================
//	Declaracion de funciones
// ===============================
void mostrarMensajeInicial();
void mensajeNombreNoValido();
void mensajeApellidoNoValido();
void mensajeEdadNoValida();
void mostrarResultados();
float edadPromedio(int edadesArr[], int LongArr);
int posEdadMax(int edadesArr[], int LongArr);
int posEdadMin(int edadesArr[], int LongArr);
bool verificarNombreApellido(char name[]);
bool verificarEdad(int edad);

// ===============================
//	Principal
// ===============================
int main(){
	// Arreglos para almacenar la data
    char nombres[CANT_PERSONAS][MAX_LEN];
    char apellidos[CANT_PERSONAS][MAX_LEN];
    int edades[CANT_PERSONAS];
    // Para validaciones
    bool checkNombre = false;
    bool checkApell = false;
    bool checkEdad = false;
    char tmp[MAX_LEN];
    int tmp_int;
    // Varuables auxiliares
    float edadProm, edadMin, edadMax;
    int i, posMin, posMax;


    mostrarMensajeInicial();

    // For principal
    for (i = 0; i<CANT_PERSONAS; i++){
        printf(" -> Persona %d de %d\n", i+1, CANT_PERSONAS);
        // -------------------------------------------------
	    do {
	      	printf("    Ingrese nombre: ");
	      	scanf("%s", tmp);
	      	getchar();
	      	checkNombre = verificarNombreApellido(tmp);
	      	if (checkNombre==false){
	      		mensajeNombreNoValido();
	      	}
	    }while( checkNombre == false );
	    strcpy(nombres[i], tmp);
	    tmp[0] = '\0'; // Reset tmp
	    // -------------------------------------------------
	    do {
	      	printf("    Ingrese apellido: ");
	      	scanf("%s", tmp);
	      	getchar();
	      	checkApell = verificarNombreApellido(tmp);
	      	if (checkApell==false){
				mensajeApellidoNoValido();
	      	}
	    }while( checkApell == false );
	    strcpy(apellidos[i], tmp);
	    tmp[0] = '\0'; // Reset tmp
	    // -------------------------------------------------
	    do {
	      	printf("    Ingrese edad: ");
	      	scanf("%d", &tmp_int);
	      	getchar();
	      	checkEdad = verificarEdad(tmp_int);
	      	if (checkEdad==false){
				mensajeEdadNoValida();
	      	}
	    }while( checkEdad == false );
	    edades[i] = tmp_int;
	    tmp_int = -1; // Reset tmp_int
	    // -------------------------------------------------
        printf("\n");
    }
    
    // Resultados: 
    printf("\n----------------------------------------------------");
    printf("\n\t\t    Estadisticas");
    printf("\n----------------------------------------------------");
    edadProm = edadPromedio(edades, CANT_PERSONAS);
    printf("\n\t-> Promedio de edad: %.1f anios ", edadProm);
    
    posMin = posEdadMin(edades, CANT_PERSONAS);
    printf("\n\t-> Persona mas joven: %s %s ", nombres[posMin], apellidos[posMin]);
    printf("\n\t   Edad: %d anios ", edades[posMin]);
    
    posMax = posEdadMax(edades, CANT_PERSONAS);
    printf("\n\t-> Persona mas vieja: %s %s ", nombres[posMax], apellidos[posMax]);
    printf("\n\t   Edad: %d anios", edades[posMax]);
    printf("\n----------------------------------------------------\n");

    return 0;
}



// ===============================
//	Implementacion de funciones
// ===============================

// Imprime el mensaje con las reglas
void mostrarMensajeInicial(){
      printf(".-----------------------------------------------------.");
    printf("\n|                 Bienvenido usuario!                 |");
    printf("\n|           A continuacion, debera ingresar           |");
    printf("\n|               los datos de %d personas.             |", CANT_PERSONAS);
    printf("\n| REGLAS:                                             |");
    printf("\n|  - Los nombres y apellidos comienzan con mayuscula  |");
    printf("\n|    y el resto en minusculas.                        |");
    printf("\n|  - No ingrese caracteres especiales.                |");
    printf("\n|  - La edad es un numero entre 0 y 130.              |");
    printf("\n'-----------------------------------------------------'\n\n");
}

void mensajeNombreNoValido(){
	printf("\n    .------------------------------------------------------.");
	printf("\n    |  El nombre ingresado no es valido. Intente de nuevo  |");
	printf("\n    '------------------------------------------------------'\n\n");
};


void mensajeApellidoNoValido(){
	printf("\n    .--------------------------------------------------------.");
	printf("\n    |  El apellido ingresado no es valido. Intente de nuevo  |");
	printf("\n    '--------------------------------------------------------'\n\n");
};

void mensajeEdadNoValida(){
	printf("\n    .----------------------------------------------------.");
	printf("\n    |  La edad ingresada no es valida. Intente de nuevo  |");
	printf("\n    '----------------------------------------------------'\n\n");
};

// Funcion para obtener la edad promedio
float edadPromedio(int edadesArr[], int LongArr){
    int i, acum = 0;
    float prom = 0;

    for (i = 0; i < LongArr; i++){
    	acum = acum + (int)edadesArr[i];
    }
    prom = (float)acum / (float)LongArr;
    return prom;
}

// Funcion para obtener la posicion de la edad maxima
int posEdadMax(int edadesArr[], int LongArr){
    int i, pos;
    int k = 0;
    int max = edadesArr[k];

    for (i = 0; i < LongArr; i++){
        if (edadesArr[i] > max){
            max = (int)edadesArr[i];
            pos = i;
        }
    }
    return pos;
}

// Funcion para obtener la posicion de la edad minima 
int posEdadMin(int edadesArr[], int LongArr){
    int i, pos;
    int k = 0;
    int min = edadesArr[k];

    for (i = 0; i < LongArr; i++){
        if (edadesArr[i] < min){
            min = (int)edadesArr[i];
            pos = i;
        }
    }
    return pos;
}

// Funcion que valida el nombre y el apellido
bool verificarNombreApellido(char name[]){
	int i=0, c_ascii, flag=1; 
	char c;

	while( name[i]!='\0' ){
		c = name[i]; 
		c_ascii = (int)name[i];

		// Si esta entre [A-Z] y [a-z] 
		if ( (c_ascii>64 && c_ascii<91) || (c_ascii>96 && c_ascii<123) ){
			// Primer letra
			if ( i==0 ){
				// Si el char no esta entre A && Z  -> false
				if ( !(c_ascii>64 && c_ascii<91) ){
					flag = 0; break;
				}
			}
			// ... resto de letras
			else {
				// Si el char no esta entre a && z -> false
				if ( !(c_ascii>96 && c_ascii<123) ){
					flag = 0; break;
				}
			}
		}
		// Si NO esta entre [A-Z] y [a-z]
		else {
			flag = 0; break;
		}

		i++;

	} // End while


	// Validacion del flag
	if (flag==1){
		return true;
	}
	else{
		return false;
	}
	
}

// Funcion que valida la edad
bool verificarEdad(int edad){
	char c;
	char edadStr[10];
	int flag=1, i=0, c_asc;

	sprintf(edadStr, "%d", edad);
	//  Numero de mas de 3 cifras
	if ( strlen(edadStr)>3 ){
		flag = 0;
	}
	else {
		// Verificamos que todos sean numeros
		while( edadStr[i]!='\0' ){
			c_asc = (int)edadStr[i];
			if ( c_asc<48 || c_asc>57 ){
				flag = 0; break;
			}
			i++;
		}
		// Si todos son numeros, verificamos que esten entre 0 y 130
		if (flag==1){ // No hubo error en el while
			if ( edad<0 || edad>130 ){
				flag = 0;
			}
		}
	}

	// Validacion del flag
	if (flag==1){
		return true;
	}
	else{
		return false;
	}

}